## Introduction

Hi, I'm Aster! My pronouns are she / her, and I am a trans woman.

I pretty much put enough of my life online at this point that anything short of just stating it outright would feel like an easy invitation for people to feel clever and out me. Whatever. The only alternative I could see, which I considered for a short time, would be for me to let morerunes fade into obscurity and create a new online persona. However, I've always gone by morerunes online, and I have no intention of abandoning this identity / brand.

I'm not going to go into a lot of details because it's very personal, but this has been in the works for a very long time.

## Why Write About It?

The reason I put this website up was because I wanted to have a place to showcase projects that I'm working on. I find it helps to have a place to just shout out what I'm doing, both as motivation for me to actually do it, and as a form of self-care (i.e., "I am worth showcasing, my projects matter"). When I started showcasing my projects in the beginning, it wasn't about demonstrating any kind of technical prowess or bragging about skills or anything, it was because I grew up alongside the internet and I admired people that showcased their projects and themselves without shame. I knew that no matter what I do, I will learn and grow and change over time, and there was a chance that whatever I looked back on I would feel I had outgrown it.

So why am I writing about being trans? I don't necessarily think it's very interesting for most people to read about, but if I don't put it out there then I won't have anywhere to point people to if I get questions about it. Also, it's relevant to my creative projects. As I got older I slowly lost the ability to express myself because I was hiding from myself. It was eating at me and eventually it began affecting my ability to perform my job effectively. In August 2022 I made the decision to stop living in the closet, and I haven't looked back since. I can't promise I'll have the time or creative energy to publish regular updates, new mods, new music, coding projects, or even blog posts; but I'll definitely be happier.

In short, my other projects were put on hold because of a more pressing project &dash; me.

## Closing Thoughts

I'm keeping this brief. I don't have much to say other than - if you are trans and you are in the closet because you are afraid that to transition (socially, medically, or otherwise) would cause harm or stress or undue pressure to anyone in your life, please know that you aren't a burden for existing and being yourself. You deserve to live happily, whatever that means for you. Everybody's journey is different, and you don't need to be trans to question your gender identity, expression, or whatever. Life is short, don't be afraid to live it.

I'm going to keep working on myself and progressing and changing, but morerunes isn't going anywhere. I firmly believe my coolest projects lie in my future, and I am ready to fulfill what I set out to do over a decade ago. Live unapologetically.

Stay faithful ye residents of Rune Land ;)

By the way, I've also got an [RSS feed](/rss.xml) if you want to get notified of updates (does anybody use RSS still?)

![trans pride flag](/images/transpride.png)
